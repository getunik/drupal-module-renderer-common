<?php

namespace Drupal\renderer_inxmail_subscription;

class InxmailSubscription extends \Drupal\renderer_plugins\BaseRenderer
{
	public function execute($wrapper, $view_mode, $args)
	{
		$match = NULL;
		$attributes = array();
		foreach ($args as $name => $value)
		{
			if (preg_match('/^attr_(.*)/', $name, $match))
			{
				$attributes[$match[1]] = $value;
			}
		}

		// delegate rendering to the inxmail_newsletter_registration module
		return array(
			'content' => array(
				'#theme' => 'inxmail_newsletter_registration',
				'#list_name' => isset($args['list_name']) ? $args['list_name'] : 'list_name_not_set',
				'#source' => isset($args['source']) ? $args['source'] : '',
				'#button_label' => isset($wrapper->field_button_text) ? $wrapper->field_button_text->value() : (isset($args['button_label']) ? $args['button_label'] : NULL),
				'#input_label' => isset($args['input_label']) ? $args['input_label'] : NULL,
				'#inx_attributes' => $attributes,
			),
		);
	}
}
