
(function ($) {

	function triggerSocialEvent(triggerElement, socialData) {
		var e = $.Event('social-interaction');
		e.socialData = socialData;

		triggerElement.trigger(e);
	}

	Drupal.behaviors.socialShare = {
		attach: function(context, settings) {
			$('.renderer--social-share', context).once('socialShare', function () {

				if ($('.sharing-facebook', context).length > 0 && window.FB === undefined) {
					window.fbAsyncInit = function() {
						// init the FB JS SDK
						FB.init({
							appId      : settings.facebook_app_id, // App ID from the app dashboard
							status     : false,          // Check Facebook Login status
							xfbml      : false,           // Look for social plugins on the page
						});
					};

					// Load the SDK asynchronously
					(function(d, s, id){
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) {return;}
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/all.js";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
				}

				var baseOptions = $(this).data();

				$('.sharing-facebook', this).click(function (e) {
					e.preventDefault();
					e.stopPropagation();

					var options = $.extend({ url: $(this).attr('href') }, $(this).data(), baseOptions);

					triggerSocialEvent($(this), {
						network: 'Facebook',
						action: 'Sharing',
						target: options.url,
					});

					FB.ui({
						method: 'share',
						href: options.url,
					});
				});

				$('.sharing-twitter', this).click(function (e) {
					e.preventDefault();
					e.stopPropagation();

					var options = $.extend({ url: $(this).attr('href') }, $(this).data(), baseOptions);
					var twitterUrl = 'https://twitter.com/share?url=' + escape(options.url);
					twitterUrl += '&text=' + encodeURIComponent(options.text);
					if (options.tags) {
						twitterUrl += '&hashtags=' + encodeURIComponent(options.tags);
					}

					triggerSocialEvent($(this), {
						network: 'Twitter',
						action: 'Sharing',
						target: options.url,
					});

					window.open(twitterUrl, "Twitter", "width=500,height=250");
				});

				// @see https://developers.google.com/+/web/share/
				// @see https://developers.google.com/+/web/snippet/
				$('.sharing-google', this).click(function (e) {
					e.preventDefault();
					e.stopPropagation();

					var options = $.extend({ url: $(this).attr('href') }, $(this).data(), baseOptions);
					var googleUrl = 'https://plus.google.com/share?url=' + encodeURIComponent(options.url);

					triggerSocialEvent($(this), {
						network: 'Google+',
						action: 'Sharing',
						target: options.url,
					});

					window.open(googleUrl, 'Share to Google+','width=600,height=460,menubar=no,location=no,status=no');
				});

			});
		}
	};

})(jQuery);
