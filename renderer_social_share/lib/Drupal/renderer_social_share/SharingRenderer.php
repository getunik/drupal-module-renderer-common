<?php

namespace Drupal\renderer_social_share;

class SharingRenderer extends \Drupal\renderer_plugins\BaseRenderer
{
	protected static $modules = array(
		'facebook' => 'Facebook',
		'twitter' => 'Twitter',
		'google' => 'Google+',
		'email' => 'Email',
	);

	public function execute($wrapper, $view_mode, $args)
	{
		$enabled_modules = isset($args['modules']) ? (is_array($args['modules']) ? $args['modules'] : explode(',', $args['modules'])) : array_keys(self::$modules);
		$sharing_url = $this->requiredArg($args, 'sharing_url', $GLOBALS['base_url'] . request_uri());
		if (strpos($sharing_url, 'http') !== 0)
		{
			$sharing_url = url($sharing_url, array('absolute' => TRUE));
		}

		$modulePath = drupal_get_path('module', 'renderer_social_share');
		drupal_add_js($modulePath . '/js/social_share.behaviors.js');

		return array(
			'sharing_url' => $sharing_url,
			'modules' => $this->getSharingModules($enabled_modules, $args),
		);
	}

	/**
	 * This function generates an associative array with enabled module names as keys and the corresponding
	 * value is an associative array with two values:
	 * - 'class': the CSS class name that should be applied to the sharing option in the template
	 * - 'attributes': a string containing all the relevant attributes for the sharing option - this includes
	 *     the 'href' attribute and some implementation-specific data attributes.
	 */
	protected function &getSharingModules($enabled_modules, $args)
	{
		$modules = array();
		foreach ($enabled_modules as $module)
		{
			$module = trim($module);
			$attrs = array(
				'href' => '#',
			);

			switch ($module)
			{
				case 'twitter':
					$attrs['data-text'] = $this->requiredArg($args, 'twitter_text');
					$attrs['data-tags'] = $this->requiredArg($args, 'twitter_tags', '');
					break;

				case 'email':
					$attrs['href'] = $this->getMailtoLink($args);
					break;
			}

			$params = array(
				'name' => t(self::$modules[$module]),
				'class' => 'sharing-' . $module,
				'attributes' => '',
			);
			foreach ($attrs as $k => $v)
			{
				$params['attributes'] = $params['attributes'] . $k . '="' . $v . '" ';
			}

			$modules[$module] = $params;
		}

		return $modules;
	}

	protected function getMailtoLink($args)
	{
		$parts = array(
			'subject' => $this->requiredArg($args, 'mail_subject'),
			'body' => $this->requiredArg($args, 'mail_body', ''),
		);

		$query = array();
		foreach ($parts as $key => $value)
		{
			$query[] =  $key . '=' . rawurlencode($value);
		}

		return 'mailto:' . $this->requiredArg($args, 'mail_to', '') . '?' . implode('&amp;', $query);
	}

	protected function requiredArg($args, $name, $default = NULL)
	{
		if (isset($args[$name]) && !empty($args[$name]))
		{
			return $args[$name];
		}

		if ($default !== NULL)
		{
			return $default;
		}

		drupal_set_message(t('Missing required parameter "@name" in social sharing renderer', array('@name' => $name)), 'error', TRUE);
		return NULL;
	}
}
