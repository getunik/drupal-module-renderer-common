<?php
	//dpm(get_defined_vars(), "template vars");
?>

<div class="renderer--social-share" data-url="<?php print $sharing_url; ?>">
	<?php foreach ($modules as $key => $params): ?>
		<a class="<?php print $params['class']; ?>" <?php print $params['attributes']; ?>><?php print ucfirst($params['name']); ?></a>
	<?php endforeach; ?>
</div>
